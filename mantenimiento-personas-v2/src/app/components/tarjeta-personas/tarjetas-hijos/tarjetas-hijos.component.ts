import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PersonaInputDto } from 'src/app/models';
import { CheckboxVerDetalleService } from 'src/app/services/checkbox-ver-detalle.service';
import { PersonasService } from 'src/app/services/persona.service';
import { AlertBorradoComponent } from '../../ventanas-modales/alert-borrado/alert-borrado.component';
import { FormularioComponent } from '../../ventanas-modales/formulario/formulario.component';

@Component({
  selector: 'app-tarjetas-hijos',
  templateUrl: './tarjetas-hijos.component.html',
  styleUrls: ['./tarjetas-hijos.component.scss']
})
export class TarjetasHijosComponent implements OnInit {

  @ViewChild("detalle") (MatButton) botonDetalle: MatButton
  
  @Input() persona: PersonaInputDto;
  @Output() updatePersonas = new EventEmitter<PersonaInputDto[]>();

  constructor(private personaService: PersonasService,
              private dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private detalleService: CheckboxVerDetalleService) { 
    this.persona = {} as PersonaInputDto;
  }

  ngOnInit(): void {
  }

  editar(){
    let dialogRef = this.dialog.open(FormularioComponent, {data: this.persona})

    dialogRef.afterClosed().subscribe( () => 
      this.personaService.cargarPersonas().subscribe( (data) => {
        this.updatePersonas.emit(data);
      })
    )
  }

  baja(){
    let dialogRef = this.dialog.open(AlertBorradoComponent)

    dialogRef.beforeClosed().subscribe( (data) => {
      if(data){
        this.personaService.deletePersona(this.persona.id_persona).subscribe( () => {
          this.personaService.cargarPersonas().subscribe( (data) => {
            this.updatePersonas.emit(data);
          })
        })
      }
    })
  }

  pulsaDetalle(){
    if(!this.detalleService.getDetalle()){
      this._snackBar.open("Pulse el check para ver detalle", "", {
        duration: 5000,
        verticalPosition: 'top', // 'top' | 'bottom'
        horizontalPosition: 'center', //'start' | 'center' | 'end' | 'left' | 'right'
      });
    }else{
      this._snackBar.dismiss()
    }
  }
}
