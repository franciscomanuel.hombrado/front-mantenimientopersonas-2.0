import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckboxVerDetalleService {

  private verDetalle: boolean

  constructor() { }

  setDetalle(bool: string){
    if(bool == "true"){
      this.verDetalle = true
    }else{
      this.verDetalle = false
    }
  }

  getDetalle(): boolean{
    return this.verDetalle
  }
}
