import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Persona } from 'src/app/models';
import { PersonasService } from 'src/app/services/persona.service';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  hidePass = true

  register: FormGroup

  constructor(private fb: FormBuilder,
              private personaService: PersonasService,
              @Inject(MAT_DIALOG_DATA) private persona: Persona) 
  {
    this.register = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      company_email: ['', Validators.required],
      personal_email: ['', Validators.required],
      city: ['', Validators.required],
      active: ['', Validators.required],
      created_date: ['', Validators.required],
      imagen_url: ['', Validators.required],
      termination_date: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if(this.persona){
      this.register = this.fb.group({
        usuario: [this.persona.usuario, Validators.required],
        password: [this.persona.password, Validators.required],
        name: [this.persona.name, Validators.required],
        surname: [this.persona.surname, Validators.required],
        company_email: [this.persona.company_email, Validators.required],
        personal_email: [this.persona.personal_email, Validators.required],
        city: [this.persona.city, Validators.required],
        active: [this.persona.active, Validators.required],
        created_date: [this.persona.created_date, Validators.required],
        imagen_url: [this.persona.imagen_url, Validators.required],
        termination_date: [this.persona.termination_date, Validators.required]
      });
    }
  }

  limpiar(){
    this.register.reset()
  }

  onSubmit(){
    if(this.persona){
      this.personaService.putPersona(this.persona.id_persona, Persona.personaOutputDto(this.register.value)).subscribe()
    }else{
      this.personaService.postPersona(this.register.value).subscribe()
    }
  }

}
