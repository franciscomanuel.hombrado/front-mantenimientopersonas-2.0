import { Injectable} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Persona,} from '../models';
import { DetalleComponent } from '../components/detalle/detalle.component';

@Injectable({  providedIn: 'root'})
export class VentanaModalService {
    personaDetalle: Persona[]

    constructor() {
    this.personaDetalle = [];
  }

  cargarPersonaDetalle(persona: Persona, dialog: MatDialog){
    this.personaDetalle[0] = persona
    dialog.open(DetalleComponent)
  }

  getPersonaTablaDetalle(){
    return this.personaDetalle;
  }

}