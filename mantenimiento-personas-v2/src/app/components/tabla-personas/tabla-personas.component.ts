import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';

import { Persona, PersonaInputDto } from '../../models';
import { PersonasService } from 'src/app/services/persona.service';
import { FormularioComponent } from '../ventanas-modales/formulario/formulario.component';
import { AlertBorradoComponent } from '../ventanas-modales/alert-borrado/alert-borrado.component';

import { MatSnackBar } from '@angular/material/snack-bar';
import { CheckboxVerDetalleService } from 'src/app/services/checkbox-ver-detalle.service';

@Component({
  selector: 'app-tabla-personas',
  templateUrl: './tabla-personas.component.html',
  styleUrls: ['./tabla-personas.component.css']
})
export class TablaPersonasComponent {

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string[] = ['name', 
                                'surname',
                                'botones'];

  personasTabla = new MatTableDataSource<PersonaInputDto>();

  constructor(private personaService: PersonasService,
              private dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private detalleService: CheckboxVerDetalleService
              ) {  }

  ngOnInit() : void{
    this.personaService.cargarPersonas().subscribe( (data) =>
      this.personasTabla = new MatTableDataSource<PersonaInputDto>(data)
    );
  }

  alta(){
    let dialogRef = this.dialog.open(FormularioComponent)

    dialogRef.afterClosed().subscribe( () => 
      this.ngOnInit()
    )
  }

  modificar(persona: Persona){
    let dialogRef = this.dialog.open(FormularioComponent, {data: persona})

    dialogRef.afterClosed().subscribe( () => 
      this.ngOnInit()
    )
  }

  baja(id: string){
    let dialogRef = this.dialog.open(AlertBorradoComponent)

    dialogRef.beforeClosed().subscribe( (data) => {
      if(data){
        this.personaService.deletePersona(id).subscribe( () => 
          this.ngOnInit()
        )
      }
    })
  }

  pulsaDetalle(){
    if(!this.detalleService.getDetalle()){
      this._snackBar.open("Pulse el check para ver detalle", "", {
        duration: 5000,
        verticalPosition: 'top', // 'top' | 'bottom'
        horizontalPosition: 'center', //'start' | 'center' | 'end' | 'left' | 'right'
      });
    }else{
      this._snackBar.dismiss()
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.personasTabla.filter = filterValue.trim().toLowerCase();
  }
}