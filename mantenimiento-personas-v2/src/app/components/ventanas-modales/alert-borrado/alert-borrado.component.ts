import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert-borrado',
  templateUrl: './alert-borrado.component.html',
  styleUrls: ['./alert-borrado.component.scss']
})
export class AlertBorradoComponent implements OnInit {

  titulo: string
  mensaje: string

  ngOnInit(): void {
    this.titulo = "Confirmación borrado"
    this.mensaje = "¿Desea eliminarlo?"
  }

}
