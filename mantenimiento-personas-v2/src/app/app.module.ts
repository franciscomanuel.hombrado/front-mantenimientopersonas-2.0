import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DetalleComponent } from './components/detalle/detalle.component';
import { TablaPersonasComponent } from './components/tabla-personas/tabla-personas.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { PersonasService } from './services/persona.service';
import { VentanaModalService } from './services/ventana-modal.service';
import { MenuComponent } from './components/menu/menu.component';

import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import { TarjetasPadreComponent } from './components/tarjeta-personas/tarjetas-padre/tarjetas-padre.component';
import { TarjetasHijosComponent } from './components/tarjeta-personas/tarjetas-hijos/tarjetas-hijos.component';

import {MatCardModule} from '@angular/material/card';
import { AlertBorradoComponent } from './components/ventanas-modales/alert-borrado/alert-borrado.component';
import { FormularioComponent } from './components/ventanas-modales/formulario/formulario.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { CheckboxVerDetalleService } from './services/checkbox-ver-detalle.service';

import { VerDetalleGuard } from './shared/guards/ver-detalle.guard';

import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    DetalleComponent,
    TablaPersonasComponent,
    MenuComponent,
    BienvenidaComponent,
    TarjetasPadreComponent,
    TarjetasHijosComponent,
    AlertBorradoComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    FlexLayoutModule,

    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,

    MatCardModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  providers: [PersonasService, VentanaModalService, CheckboxVerDetalleService, VerDetalleGuard],
  bootstrap: [AppComponent],
  entryComponents: [FormularioComponent, AlertBorradoComponent, DetalleComponent]
})
export class AppModule { }
