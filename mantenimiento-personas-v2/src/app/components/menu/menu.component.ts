import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { CheckboxVerDetalleService } from 'src/app/services/checkbox-ver-detalle.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent{
  @ViewChild(MatCheckbox) checkbox: MatCheckbox

  mobileQuery: MediaQueryList;

  onTarjetas = false

  fillerNav = [
    {name:"Listar formato tabla", route: "/formatoTabla"},
    {name:"Listar formato detalle", route: "/formatoTarjetas"}
  ]

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
              private checkBoxVerDetalleService: CheckboxVerDetalleService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
  }

  ngAfterViewInit(): void{
    this.setCheckbox()
  }

  setCheckbox(){
    this.checkBoxVerDetalleService.setDetalle(this.checkbox._getAriaChecked())
  }

}
