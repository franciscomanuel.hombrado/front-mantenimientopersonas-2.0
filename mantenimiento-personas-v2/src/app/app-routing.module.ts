import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { MenuComponent } from './components/menu/menu.component';
import { TablaPersonasComponent } from './components/tabla-personas/tabla-personas.component';
import { TarjetasPadreComponent } from './components/tarjeta-personas/tarjetas-padre/tarjetas-padre.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { VerDetalleGuard } from './shared/guards/ver-detalle.guard';

const routes: Routes = [
  
  {
    path: '', component: MenuComponent, children : [
      {
        path: 'welcome', component : BienvenidaComponent
      },

      {
        path: 'formatoTabla', component: TablaPersonasComponent
      },
    
      {
        path: 'formatoTarjetas', component: TarjetasPadreComponent
      },

      {
        path: 'detalle/:id_persona', component: DetalleComponent, canActivate: [VerDetalleGuard]
      },

      {
        path: '**', pathMatch: 'full', redirectTo: 'welcome'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes/*, {onSameUrlNavigation: 'reload'}*/)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
