import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PersonaInputDto } from 'src/app/models';
import { PersonasService } from 'src/app/services/persona.service';
import { FormularioComponent } from '../../ventanas-modales/formulario/formulario.component';

@Component({
  selector: 'app-tarjetas-padre',
  templateUrl: './tarjetas-padre.component.html',
  styleUrls: ['./tarjetas-padre.component.scss']
})
export class TarjetasPadreComponent implements OnInit {

  sinPersonas: boolean
  personas: PersonaInputDto[]

  constructor(private personaService: PersonasService,
              private dialog: MatDialog) {
    this.personas = []

    /*this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };*/
  }

  cargarPersonas(personas : PersonaInputDto[]){
    this.personas = personas

    if(this.personas.length > 0){
      this.sinPersonas = false
    }else{
      this.sinPersonas = true
    }
  }

  ngOnInit(): void {
    this.personaService.cargarPersonas().subscribe( (data) =>{
      this.personas = data

      if(this.personas.length > 0){
        this.sinPersonas = false
      }else{
        this.sinPersonas = true
      }
    })
  }

  addPersona(){
    let dialogRef = this.dialog.open(FormularioComponent);

    dialogRef.afterClosed().subscribe( () => 
      this.ngOnInit()
    )
  }

}
