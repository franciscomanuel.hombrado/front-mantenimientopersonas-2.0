import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { CheckboxVerDetalleService } from 'src/app/services/checkbox-ver-detalle.service';

@Injectable({
  providedIn: 'root'
})
export class VerDetalleGuard implements CanActivate {

  constructor(private checkboxVerDetalleService: CheckboxVerDetalleService){}

  canActivate(): boolean {
      return this.checkboxVerDetalleService.getDetalle()
  }

  
  
}
