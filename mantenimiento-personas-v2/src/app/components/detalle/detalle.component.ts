import { Component, OnInit } from '@angular/core';

import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';

import { Persona, PersonaInputDto } from 'src/app/models';
import { PersonasService } from 'src/app/services/persona.service';

@Component({
  selector: 'app-ventana-modal',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {

  displayedColumns: string[] = ['usuario',
                                'password',
                                'name',
                                'surname',
                                'company_email',
                                'personal_email',
                                'city',
                                'active',
                                'created_date', 
                                'imagen_url',
                                'termination_date'];

  personaTablaDetalle = new MatTableDataSource<PersonaInputDto>();

  constructor(private route: ActivatedRoute,
              private personaService: PersonasService) {  }

  ngOnInit(): void {    
    this.route.params.subscribe(params => {
      this.personaService.cargarPersona(params['id_persona']).subscribe( (data) => {

        let persona: Persona[] = []

        persona[0] = Persona.personaOutputDto(data)
        
        this.personaTablaDetalle = new MatTableDataSource<PersonaInputDto>(persona);
      })
    })        
  }

}
